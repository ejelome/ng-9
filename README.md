ng-9
====

Angular 9

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [ng-9](#ng-9)
    - [Setup](#setup)
        - [System](#system)
            - [npm](#npm)
        - [Project](#project)
            - [Dependencies](#dependencies)
                - [@angular/cli](#angularcli)
    - [Usage](#usage)
        - [Build](#build)
        - [Serve](#serve)
    - [License](#license)
    - [Boilerplate](#boilerplate)
        - [Base](#base)
    - [Date](#date)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

Setup
-----

### System ###

#### npm ####

*See [npm](https://www.npmjs.com).*

### Project ###

#### Dependencies ####

##### @angular/cli #####

``` shell
$ npm i -g @angular/cli
```

**Note:** `i` is a shortcut for `install`.

-------------------------------------------------------------------------------

Usage
-----

### Build ###

``` shell
$ ng build --prod
```

### Serve ###

``` shell
$ ng serve -o
```

-------------------------------------------------------------------------------

License
-------

`ng-9` is licensed under [MIT].

-------------------------------------------------------------------------------

Boilerplate
-----------

### Base ###

1. Create a new project:

   ``` shell
   $ ng new project
   ```

   **Note:** Since this is only for the basics, pick plain `CSS`, e.g.:

   - `? Would you like to add Angular routing?` `Yes`
   - `? Which stylesheet format would you like to use?` `CSS`

2. Change dir to project:

   ``` shell
   $ cd project
   ```

3. Run web server:

    ``` shell
    $ ng serve -o
    ```

    **Notes:**

    - `s` is a shortcut for `serve`
    - `-o` automatically opens the default browser

    Then visit http://localhost:8000.

4. Update `project/src/app/app.component.html`:

   ``` html
   <header>
       <a routerLink="/">Project</a>
   </header>

   <nav>
       <ul>
           <li>
               <a routerLink="/posts">Posts</a>
           </li>
       </ul>
   </nav>

   <main>
       <router-outlet></router-outlet>
   </main>
   ```

   **Note:** The `routerLink` not `href` is the one used to link to routes in the app.

5. Update `project/src/styles.css`:

   ``` css
   /* You can add global styles to this file, and also import other style files */
   header {
       background: #FBF1A9; /* light-yellow */
       padding: 1rem;
   }
   ```

   **Note:** `src/styles.css` is the site-wide style affecting the whole app.

6. Create a new component for homepage:

   ``` shell
   $ ng g c home
   ```

   **Notes:**

   - `g` is a shortcut for generate
   - `c` is a shortcut for component

6. Create a new component for posts:

   ``` shell
   $ ng g c posts
   ```

7. Update `project/src/app/app-routing.module.ts`:

   ``` typescript
   import { NgModule } from '@angular/core';
   import { Routes, RouterModule } from '@angular/router';
   import { HomeComponent } from './home/home.component';
   import { PostsComponent } from './posts/posts.component';

   const routes: Routes = [
     {
       path: '',
       component: HomeComponent,
     },
     {
       path: 'posts',
       component: PostsComponent,
     },
   ];

   @NgModule({
     imports: [RouterModule.forRoot(routes)],
     exports: [RouterModule]
   })

   export class AppRoutingModule { }
   ```

   **Notes:**

   - Component names are PascalCased and suffixed with `Component` (e.g. `HomeComponent` for `home`)
   - The `path` corresponds to the `routerLink` used in the components
   - Whichever `path` comes first, is executed first, same `path` after are ignored

8. Update `project/src/app/home/home.component.html`:

   ``` html
   <h1>Home</h1>

   <button (click)="countClick()">Click me</button>
   <p>You clicked {{ clickCounter }} times.</p>
   ```

9. **Notes:**

   - `(click)` is the event handler
   - `clickCount()` is the callback method (wrapped in quotes)
   - `clickCounter` is a property of the current component
   - This basic event handling is a one-way data binding

10. Update `project/src/app/home/home.component.ts`:

    ``` typescript
    import { Component, OnInit } from '@angular/core';

    @Component({
      selector: 'app-home',
      templateUrl: './home.component.html',
      styleUrls: ['./home.component.css']
    })

    export class HomeComponent implements OnInit {
      clickCounter: number = 0;

      constructor() { }

      ngOnInit(): void {
      }

      countClick() {
        this.clickCounter += 1;
      }
    }
    ```

11. Update `project/src/app/home/home.component.css`:

    ``` typescript
    h1 {
        color: #19A974; /* green */
    }
    ```

    **Note:** `*.component.css` only affects its child elements.

12. Update `project/src/app/home/home.component.html`:

    ``` html
    <!-- Append the following after the previous markup: -->
    <input [(ngModel)]="name" />
    <p>Your name is {{ name }}.</p>
    ```

13. Update `project/src/app/app.module.ts`:

    ``` typescript
    import { BrowserModule } from '@angular/platform-browser';
    import { NgModule } from '@angular/core';

    import { AppRoutingModule } from './app-routing.module';
    import { AppComponent } from './app.component';
    import { HomeComponent } from './home/home.component';
    import { PostsComponent } from './posts/posts.component';
    import { FormsModule } from '@angular/forms';

    @NgModule({
      declarations: [
        AppComponent,
        HomeComponent,
        PostsComponent,
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
      ],
      providers: [],
      bootstrap: [AppComponent],
    })

    export class AppModule { }
    ```

   **Note:** `FormsModule` is required to make two-way data binding to work (via `ngModel`).

14. Update `project/src/app/home/home.component.ts`:

    ``` typescript
    // ...
    export class HomeComponent implements OnInit {
      name: string = '';
      // ...
    }
    ```

14. Update `project/src/app/home/home.component.html`:

    ``` html
    <!-- Append the following after the previous markup: -->
    <ng-template [ngIf]="clickCounter >= 10"
                 [ngIfElse]="none">You have exceeded the limit! ({{ clickCounter }})</ng-template>
    <ng-template #none>You are way below the limit! ({{ clickCounter }})</ng-template>
    ```

    **Notes:**

    - `ng-template` is specific feature for Angular 8 and above
    - `[ngIf]` and `[ngIfElse]` are conditional directives on templates
    - `ng-template` with the conditions will render its block if the condition is `true`
    - `ng-template` with the `#none` referencing `[ngIfElse]` value renders its block if the condition is `false`

15. Update `project/src/app/home/home.component.html`:

    ``` html
    <!-- Append the following after the previous markup: -->
    <p [style.color]="clickCounter >= 3 ? '#F4F4F4' : '#777777'"
       [ngStyle]="{
           'background': clickCounter >= 3 ? '#777777' : '#F4F4F4'
       }"
       [class.active]="clickCounter >= 3"
       [ngClass]="setClasses()">I'll become
           <em>bg:gray</em>,
           <em>fg:white</em> and
           <em>bordered</em>
           in the count of three! ({{ clickCounter }})</p>
    ```

    **Note:**

    - `[style.<property>]` (e.g. `[style.color]`) is similar to JS (e.g. `element.style.color`)
    - `[ngStyle]` is used for inline style binding which can handle multiple inline styles
    - `[class.<className>]` (e.g. `[class.active]`) is simply assigning a class based on a condition
    - `[ngClass]` is used to assign multiple classes using module methods

16. Update `project/src/app/home/home.component.ts`:

    ``` typescript
    // ...
    export class HomeComponent implements OnInit {
      // ...
      setClasses() {
        return {
          active: this.clickCounter > 2,
          inactive: this.clickCounter < 3,
        }
      }
    }
    ```

17. Update `project/src/app/home/home.component.css`:

    ``` css
    h1 {
        color: #19A974; /* green */
    }

    .active {
        border: #111111 solid 5px;
    }

    .inactive {
      background: #777777;
      color: #333333;
    }
    ```

18. Create a new service:

    ``` shell
    $ ng g s http
    ```

    **Notes**:

    - `s` is a shortcut for service
    - A generated service is not created with its own directory and is put on the project level

19. Update `project/src/app/http.service.ts`:

    ``` typescript
    import { Injectable } from '@angular/core';

    @Injectable({
      providedIn: 'root'
    })

    export class HttpService {
      constructor() { }

      sayHi() {
        console.log("Hello World!");
      }
    }
    ```

20. Update `project/src/app/posts/posts.component.ts`:

    ``` typescript
    import { Component, OnInit } from '@angular/core';
    import { HttpService } from '../http.service';

    @Component({
      selector: 'app-posts',
      templateUrl: './posts.component.html',
      styleUrls: ['./posts.component.css']
    })

    export class PostsComponent implements OnInit {
      constructor(private _http: HttpService) { }

      ngOnInit(): void {
        this.__http.sayHi();
      }
    }
    ```

    **Notes:**

    - Service names are PascalCased and suffixed with `Service` (e.g. `HttpComponent` for `http`)
    - `private _<service>: <Service>` (e.g. `private _http: HttpService`) in `constructor` enables dependency injection
    - The service can then be used throughout the component logic
    - `ngOnInit` is one of the component's lifecycle hook which executes when the component is loaded

21. Update `project/src/app/http.service.ts`:

    ``` typescript
    import { Injectable } from '@angular/core';
    import { HttpClient } from '@angular/common/http';

    @Injectable({
      providedIn: 'root'
    })

    export class HttpService {
        constructor(private http: HttpClient) { }

      getPosts() {
          return this.http.get('https://jsonplaceholder.typicode.com/posts');
      }
    }
    ```

    **Note:** `HttpClient` is used to connect on public APIs.

22. Update `project/src/app/posts/posts.component.ts`:

    ``` typescript
    import { Component, OnInit } from '@angular/core';
    import { HttpService } from '../http.service';

    @Component({
      selector: 'app-posts',
      templateUrl: './posts.component.html',
      styleUrls: ['./posts.component.css']
    })

    export class PostsComponent implements OnInit {
      posts: Object;

      constructor(private _http: HttpService) { }

      ngOnInit(): void {
        this
          ._http
          .getPosts()
          .subscribe(data => {
            this.posts = data;
            console.log(this.posts);
          });
      }
    }
    ```

    **Note:** The `getPosts()` returns an Observable which we then can `subscribe` to.

23. Update `project/src/app/app.module.ts`:

    ``` typescript
    import { BrowserModule } from '@angular/platform-browser';
    import { NgModule } from '@angular/core';

    import { AppRoutingModule } from './app-routing.module';
    import { AppComponent } from './app.component';
    import { HomeComponent } from './home/home.component';
    import { PostsComponent } from './posts/posts.component';
    import { FormsModule } from '@angular/forms';

    import { HttpClientModule  } from '@angular/common/http';

    @NgModule({
        declarations: [
            AppComponent,
            HomeComponent,
            PostsComponent,
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            FormsModule,
            HttpClientModule,
        ],
        providers: [],
        bootstrap: [AppComponent],
    })

    export class AppModule { }
    ```

    **Note:** The `HttpClientModule` must be imported in the project module for the `HttpClient` to work.

24. Update `project/src/app/posts/posts.component.html`:

    ```
    <h1>Posts</h1>
    <ul *ngIf="posts">
        <li *ngFor="let post of posts">
            <a href="https://jsonplaceholder.typicode.com/posts/{{ post.id }}">{{ post.title }}</a>
            <p>{{ post.body }}</p>
        </li>
    </ul>
    ```

    **Notes:** `*ngIf` (and `*ngFor`) is an implicit directive equivalent to the explicit `[ngIf]` (and `[ngFor]`).

25. Create a build to deploy app:

    ``` shell
    $ ng build --prod
    ```

    **Notes:**

    - `--prod` is a shortcut for `--configuration=production`.
    - You can then copy the build project in `dist/` (e.g. `dist/project/`)

26. Change dir to build project:

    ``` shell
    $ cd dist/project/
    ```

27. Run a web server:

    ``` shell
    $ python3 -m http.server
    ```

    Then visit <http://localhost:8000>.

-------------------------------------------------------------------------------

Date
----

`2020 Mar`

[MIT]: ./LICENSE
